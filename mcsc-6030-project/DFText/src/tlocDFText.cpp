#include "tlocDFText.h"

#include <gameAssetsPath.h>

namespace {

    core_str::String shaderPathVS("/shaders/tlocOneTextureVS.glsl");
    core_str::String shaderPathFS("/shaders/tlocOneTextureFS.glsl");
    core_str::String shaderPathFS_Deformer("/shaders/tlocOneTextureDeformerFS.glsl");
    core_str::String shaderPathFS_DFText("/shaders/tlocOneTextureFS_DFText.glsl");

    gfx_gl::texture_object_sptr g_to;
    core_str::String            g_sdfImg("images/b_sdf_1024.png");

};


// ///////////////////////////////////////////////////////////////////////
// MaterialSystemInitialize

MaterialSystemReInitialize::
MaterialSystemReInitialize(mat_sys_ptr a_matSys)
: m_matSys(a_matSys)
{ }

// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

auto
MaterialSystemReInitialize::
OnKeyPress(const tl_size, const input_hid::KeyboardEvent& a_keyEvent) const
-> event_type
{
  if (a_keyEvent.m_keyCode == input_hid::KeyboardEvent::r)
  {
    m_matSys->ReInitialize();
    return core_dispatch::f_event::Veto();
  }

  return core_dispatch::f_event::Continue();
}

// ///////////////////////////////////////////////////////////////////////
// DFText

DFText::
DFText()
: base_type("DF Text")
, m_rttScene(core_sptr::MakeShared<core_cs::ECS>())
, m_hud(core_sptr::MakeShared<core_cs::ECS>())
{ }

// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

auto
DFText::
Post_Initialize() -> error_type
{
  DoCreateSystems();
  DoCreateScene();
  DoCreateCamera();

  m_rttScene->Initialize();
  m_hud->Initialize();

  return base_type::Post_Initialize();
}

// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

void
DFText::
DoCreateCamera()
{
  auto_cref m_cameraEnt =
    GetScene()->CreatePrefab<pref_gfx::Camera>()
    .Perspective(true)
    .Near(1.0f)
    .Far(100.0f)
    .VerticalFOV(math_t::Degree(60.0f))
    .Position(math_t::Vec3f(0.0f, 0.0f, 2.0f))
    .Create(GetWindow()->GetDimensions());

  GetScene()->CreatePrefab<pref_gfx::ArcBall>().Add(m_cameraEnt);
  GetScene()->CreatePrefab<pref_input::ArcBallControl>()
    .GlobalMultiplier(math_t::Vec2f(0.01f, 0.01f))
    .Add(m_cameraEnt);

  m_meshSys->SetCamera(m_cameraEnt);
}

// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

void
DFText::
DoCreateSystems()
{
  // -----------------------------------------------------------------------
  // the RTT scene
  {
    auto p = core_io::Path(GetAssetsPath() + g_sdfImg);
    g_to = app_res::f_resource::LoadImageAsTextureObject(p);
    {
      gfx_gl::TextureObject::Params p = g_to->GetParams();
      p.MagFilter<gfx_gl::p_texture_object::filter::Linear>();
      p.MinFilter<gfx_gl::p_texture_object::filter::Linear>();
      g_to->SetParams(p);
    }

    m_rtt = core_sptr::MakeShared<gfx::Rtt>(g_to->GetDimensions());

    m_rttScene->AddSystem<gfx_cs::MaterialSystem>();
    auto meshSys = m_rttScene->AddSystem<gfx_cs::MeshRenderSystem>();
    auto rttRenderer = m_rtt->GetRenderer();
    meshSys->SetRenderer(rttRenderer);
  }

  // -----------------------------------------------------------------------
  // the HUD
  {
    auto rParams = GetRenderer()->GetParams();
    rParams.RemoveClearBit<gfx_rend::p_renderer::clear::ColorBufferBit>();

    m_hudRenderer = core_sptr::MakeShared<gfx_rend::Renderer>(rParams);

    m_hud->AddSystem<gfx_cs::MaterialSystem>();
    auto meshSys = m_hud->AddSystem<gfx_cs::MeshRenderSystem>();
    meshSys->SetRenderer(m_hudRenderer);
  }

  // -----------------------------------------------------------------------
  // main scene

  auto_cref scene = this->GetScene();
  scene->AddSystem<gfx_cs::CameraSystem>();
  scene->AddSystem<gfx_cs::ArcBallSystem>();
  auto_cref abcSys = scene->AddSystem<input_cs::ArcBallControlSystem>();

  GetKeyboard()->Register(abcSys.get());
  GetMouse()->Register(abcSys.get());
  GetKeyboard()->Register(this);

  scene->AddSystem<gfx_cs::MaterialSystem>();
  m_meshSys = scene->AddSystem<gfx_cs::MeshRenderSystem>();
  m_meshSys->SetRenderer(GetRenderer());
}

// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

void
DFText::
DoCreateScene()
{
  gfx_gl::uniform_vso   u_imgDim;
  u_imgDim->SetName("u_texDim").SetValueAs
    (g_to->GetDimensions().Cast<math_t::Vec2f32>());

  // -----------------------------------------------------------------------
  // SDF texture

  gfx_med::ImageLoaderPng il;
  il.Load(core_io::Path(GetAssetsPath() + g_sdfImg));

  auto_cref img = il.GetImage();

  auto_cref rttTo = m_rtt->AddColorAttachment(0);
  rttTo->Update(*img);

  // -----------------------------------------------------------------------
  // velocity field texture
  m_velField = core_sptr::MakeShared<gfx_gl::TextureObject>();

  auto_cref imgPtr = CreateVelocityField(core_ds::MakeTuple(8, 8));
  m_velField->Initialize(*imgPtr);

  // -----------------------------------------------------------------------
  // rtt scene
  {
    math_t::Rectf32_c rect(math_t::Rectf32_c::width(2.0f), 
                           math_t::Rectf32_c::height(2.0f));
    auto_cref q = m_rttScene->CreatePrefab<pref_gfx::Quad>().Dimensions(rect).Create();

    gfx_gl::uniform_vso u_to;
    u_to->SetName("s_texture").SetValueAs(core_sptr::ToVirtualPtr(g_to));

    gfx_gl::uniform_vso   u_to2;
    u_to2->SetName("s_velField").SetValueAs(core_sptr::ToVirtualPtr(m_velField));

    m_rttScene->CreatePrefab<pref_gfx::Material>()
      .AssetsPath(GetAssetsPath())
      .AddUniform(u_to.get())
      .AddUniform(u_to2.get())
      .AddUniform(u_imgDim.get())
      .Add(q, core_io::Path(shaderPathVS), core_io::Path(shaderPathFS_Deformer));
  }

  // -----------------------------------------------------------------------
  // HUD
  {
    math_t::Rectf32_c rect(math_t::Rectf32_c::width(0.5f), 
                           math_t::Rectf32_c::height(0.5f));
    auto_cref q = m_hud->CreatePrefab<pref_gfx::Quad>().Dimensions(rect).Create();
    q->GetComponent<math_cs::Transform>()->SetPosition(math_t::Vec3f32(1.0f, 1.0f, 0.0f));

    gfx_gl::uniform_vso u_to;
    u_to->SetName("s_texture").SetValueAs(*m_velField);

    m_hud->CreatePrefab<pref_gfx::Material>()
      .AssetsPath(GetAssetsPath())
      .AddUniform(u_to.get())
      .AddUniform(u_imgDim.get())
      .Add(q, core_io::Path(shaderPathVS), core_io::Path(shaderPathFS));
  }

  // -----------------------------------------------------------------------
  // main scene
  {
    gfx_gl::uniform_vso   u_to;
    u_to->SetName("s_texture").SetValueAs(core_sptr::ToVirtualPtr(rttTo));

    //------------------------------------------------------------------------

    math_t::Rectf_c rect(math_t::Rectf_c::width(2.0f * 0.5f),
                         math_t::Rectf_c::height(2.0f * 0.5f));

    auto_cref q = GetScene()->CreatePrefab<pref_gfx::Quad>().Dimensions(rect).Create();

    GetScene()->CreatePrefab<pref_gfx::Material>()
      .AssetsPath(GetAssetsPath())
      .AddUniform(u_to.get())
      .AddUniform(u_imgDim.get())
      .Add(q, core_io::Path(shaderPathVS), core_io::Path(shaderPathFS_DFText));
  }
}

void
DFText::
Pre_Update(sec_type a_deltaT)
{ 
  m_rttScene->Update();
  base_type::Pre_Update(a_deltaT);
  m_hud->Update();
}

void
DFText::
Pre_Render(sec_type a_deltaT)
{ 
  static bool firstRender = true;
  if (firstRender) { printf("\n"); firstRender = false; }
  { // RTT scene
    m_rttScene->Process(a_deltaT);
    m_rtt->GetRenderer()->ApplyRenderSettings();

    core_time::Timer deformTimer;
    m_rtt->GetRenderer()->Render();
    auto deformTime = deformTimer.ElapsedSeconds();

    auto to = m_rtt->begin_color_buffers()->Cast<gfx_gl::texture_object_sptr>();
    auto_cref imgPtr = to->GetImage<gfx_med::p_image::dim_2d, gfx_t::Color>();
    g_to->Update(*imgPtr);
    auto updateTime = deformTimer.ElapsedSeconds();

    printf("\rTime to deform: %.6f sec || Time to update: %.6f sec", deformTime, updateTime);
  }

  base_type::Pre_Render(a_deltaT);
}

void
DFText::
Post_Render(sec_type a_deltaT)
{ 
  { // RTT scene
    m_hud->Process(a_deltaT);
    m_hudRenderer->ApplyRenderSettings();
    m_hudRenderer->Render();
  }

  base_type::Post_Render(a_deltaT);
}

void 
DFText::
Pre_Finalize()
{
  m_rtt.reset();
  m_rttScene.reset();

  m_hudRenderer.reset();
  m_hud.reset();
}

auto
DFText::
CreateVelocityField(const gfx_t::Dimension2 a_texDim) const -> image_ptr
{
  image_ptr vf = core_sptr::MakeShared<image_ptr::value_type>();
  vf->Create(a_texDim);

  auto_cref rng = core_rng::g_defaultRNG;

  for (tl_size i = 0; i < a_texDim[0]; ++i)
  {
    for (tl_size j = 0; j < a_texDim[1]; ++j)
    {
      auto x = rng.GetRandomFloat(-1.0f, 1.0f);
      auto y = rng.GetRandomFloat(0.0f, 1.0f);

      auto range = math::MakeRangef<f32, math::p_range::Inclusive>().Get(0, 1);
      auto col = gfx_t::f_color::Encode(math_t::Vec4f32(x, y, 0, 1), range);
      gfx_t::Color colToU8(col);
      vf->SetPixel(i, j, colToU8);
    }
  }

  return vf;
}