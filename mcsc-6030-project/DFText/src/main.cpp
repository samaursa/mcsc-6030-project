#include "tlocDFText.h"

#include <gameAssetsPath.h>

using namespace tloc;

// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

int TLOC_MAIN(int , char *[])
{
  DFText prog;
  prog.Initialize(core_ds::MakeTuple(500, 500));
  prog.Run();

  //------------------------------------------------------------------------
  // Exiting
  TLOC_LOG_CORE_INFO() << "Exiting normally";

  return 0;

}
