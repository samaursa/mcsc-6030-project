#include <tlocCore/tloc_core.h>
#include <tlocMath/tloc_math.h>
#include <tlocInput/tloc_input.h>
#include <tlocGraphics/tloc_graphics.h>
#include <tlocPrefab/tloc_prefab.h>
#include <tlocApplication/tloc_application.h>

using namespace tloc;

// ///////////////////////////////////////////////////////////////////////
// MaterialSystemReInitialize

class MaterialSystemReInitialize
  : public input_hid::KeyboardListener
{
  typedef gfx_cs::material_system_vptr        mat_sys_ptr;
  typedef core_dispatch::Event                event_type;

public:
  MaterialSystemReInitialize(mat_sys_ptr a_matSys);
  event_type OnKeyPress(const tl_size, const input_hid::KeyboardEvent& a_keyEvent) const;

private:
  mat_sys_ptr m_matSys;
};
TLOC_DEF_TYPE(MaterialSystemReInitialize);

// ///////////////////////////////////////////////////////////////////////
// DFText

class DFText
  : public Application
{
public:
  typedef Application                         base_type;
  typedef base_type::error_type               error_type;
  typedef base_type::sec_type                 sec_type;

  typedef gfx_cs::mesh_render_system_vptr     mesh_sys_ptr;
  typedef gfx_med::image_sptr                 image_ptr;

public:
  DFText();

  error_type Post_Initialize()                override;
  void       Pre_Update(sec_type a_deltaT)    override;
  void       Pre_Render(sec_type a_deltaT)    override;
  void       Post_Render(sec_type a_deltaT)   override;
  void       Pre_Finalize()                   override;

  image_ptr  CreateVelocityField(gfx_t::Dimension2 a_texDim) const;

private:
  void DoCreateCamera();
  void DoCreateSystems();
  void DoCreateScene();

private:
  mesh_sys_ptr              m_meshSys;

  core_cs::ecs_sptr           m_hud;
  gfx_rend::renderer_sptr     m_hudRenderer;
  gfx_gl::texture_object_sptr m_velField;

  core_cs::ecs_sptr         m_rttScene;
  gfx::rtt_sptr             m_rtt;
};

TLOC_DEF_TYPE(DFText);