#version 330 core

// We need one out (used to be g_FragColor)
in  vec2 v_texCoord;
uniform sampler2D s_texture;
uniform sampler2D s_velField;
uniform vec2      u_texDim;

layout (location = 0) out vec4 o_color;

vec4 Decode(vec4 a_color, float kernelRadius)
{ return (a_color * kernelRadius * 2) - kernelRadius; }

vec4 Encode(vec4 a_vec, float kernelRadius)
{ return (a_vec + kernelRadius) / (kernelRadius * 2); }

void main()
{ 
  const float     kernelSize = 16; // kernel radius
        float     delta      = 1.0/u_texDim[0];

	o_color     = texture2D(s_texture, v_texCoord);

	vec4 velCol = texture2D(s_velField, v_texCoord);
  vec4 dirVec = Decode(o_color, kernelSize);

  vec4 velVec = Decode(velCol, 1);
  vec2 vel    = vec2(1, 1) * 0.5;

  bool isPos = dirVec[2] >= 0.0;
  
  // for the pixels that are on the surface, adjust the velocity directly
  // i.e. add the incoming velocity to the velocity currently on the pixel
  for (int i = -1; i <= 1; i = i + 1)
  {
    for (int j = -1; j <= 1; j = j + 1)
    {
      if (i != 0 && j != 0)
      {
        vec2 disp = vec2(delta * float(i), delta * float(j));
        vec4 n_color = texture2D(s_texture, v_texCoord + disp);
        vec4 n_dirVec = Decode(n_color, kernelSize);
        bool n_isPos = n_dirVec[2] >= 0.0;
        
        if (isPos != n_isPos)
        {
          vec2 newDirVec = dirVec.xy + vel;

          float newDis = length(newDirVec.xy);
          if (dirVec[2] < 0.0)
          { newDis = -newDis; }

          if (dot(newDirVec, dirVec.xy) < 0.0)
          { newDis = newDis * -1.0; }

          dirVec.xyz = vec3(newDirVec, newDis);

          o_color = Encode(dirVec, kernelSize);

          return;
        }   
      }
    }
  }

  // for the pixels that are not on the surface (P_n) but are neighboring to the
  // pixels that are on the surface (P_s), take P_s velocity which is now P_n's 
  // new velocity. Add a unit vector in the same direction as the velocity to 
  // adjust for the fact that P_n is 1 unit further away from the surface.
  vec2 disp = vec2(0, 0);

  if (vel[0] < 0.0) { disp[0] = -delta; }
  else              { disp[0] = delta; }

  if (vel[1] < 0.0) { disp[1] = -delta; }
  else              { disp[1] = delta; }

  vec4 n_color = texture2D(s_texture, v_texCoord + disp);
  vec4 n_dirVec = Decode(n_color, kernelSize);

  if (abs(n_dirVec[2]) < 2.0)
  {
    vec2 newDirVec = n_dirVec.xy + normalize(n_dirVec.xy);

    float newDis = length(newDirVec.xy);
    if (n_dirVec[2] < 0.0)
    { newDis = -newDis; }

    n_dirVec.xyz = vec3(newDirVec, newDis);

    o_color = Encode(n_dirVec, kernelSize);
  }
}
