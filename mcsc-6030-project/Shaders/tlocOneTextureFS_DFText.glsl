#version 330 core

// We need one out (used to be g_FragColor)
in  vec2 v_texCoord;
out vec4 o_color;
uniform sampler2D s_texture;
uniform sampler2D s_velField;
uniform vec2      u_texDim;

const float smoothing = 1.0/8.0;

vec4 Decode(vec4 a_color, float kernelRadius)
{ return (a_color * kernelRadius * 2) - kernelRadius; }

vec4 Encode(vec4 a_vec, float kernelRadius)
{ return (a_vec + kernelRadius) / (kernelRadius * 2); }

void main()
{
  const float     kernelSize = 16;
        float     delta      = 1.0/u_texDim[0];

  // NOTE: Tex coords flipped in 't' of a loaded image
	o_color = texture2D(s_texture, v_texCoord);
  vec4 o_vec = Decode(o_color, kernelSize);
  
  //if (o_vec[2] > 0.0) { o_color = vec4(1.0, 1.0, 1.0, 1.0); return; }

  bool isPos = o_vec[2] >= 0.0;
  
  for (int i = -1; i <= 1; i = i + 1)
  {
    for (int j = -1; j <= 1; j = j + 1)
    {
      if (i != 0 && j != 0)
      {
        vec2 disp = vec2(delta * float(i), delta * float(j));
        vec4 n_color = texture2D(s_texture, v_texCoord + disp);
        vec4 n_dirVec = Decode(n_color, kernelSize);
        bool n_isPos = n_dirVec[2] >= 0.0;
        
        if (isPos != n_isPos)
        {
          o_color = vec4(1.0, 1.0, 1.0, 1.0);
          return;
        }   
      }
    }
  }

  o_color = vec4(1.0, 1.0, 1.0, 1.0);
  
  if (o_vec[2] > 0) { o_color = vec4(0.0, 0.0, 0.0, 0.0); }

  //float newDis = smoothstep(0.5 - smoothing, 0.5 + smoothing, o_color[0]);
  //o_color = vec4(newDis, newDis, newDis, 1.0 - newDis);

  //float colRed    = o_color[0];
  //float colGreen  = o_color[1];
  //if (o_color[0] > 0.0)
  //{
  //  colRed = smoothstep(0.0, 0.0 + smoothing, o_color[0]);
  //}
  //else if (o_color[1] > 0.0)
  //{
  //  colGreen = smoothstep(0.0, 0.0 + smoothing, o_color[1]);
  //}

  //if (o_color[0] < 0.1)
  //{
  //  float x = o_color[0];
  //  float xNorm = x / 0.1;
  //  float xInv = 1.0 - xNorm;
  //
  //  o_color = vec3(1.0, 1.0, 1.0) * xInv;
  //}
  //else if (o_color[1] > 0.0)
  //{
  //  o_color = vec3(1.0, 1.0, 1.0);
  //}
  //else
  //{
  //  o_color = vec3(0.0, 0.0, 0.0);
  //}

  //if (IsBorder())
  //{ o_color = vec3(1.0, 1.0, 1.0); }
  //else
  //{ o_color = vec3(0.0, 0.0, 0.0); }
}
