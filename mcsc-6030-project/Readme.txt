There are two repositories which will be consolidated into one project for the final submission. The first repository is a stand-alone project that currently only works on Windows and requires the 2LoC(1) game engine. The second repository has several projects, one of them being the DF generator. This is part of the samples repository that comes with the engine.

Please note that the CMake build system is required to build the project(s).

Repository links (temporary)
- https://samaursa@bitbucket.org/samaursa/distancefieldtext (SDF text rendering)
- https://bitbucket.org/samaursa/tlocsamples (tlocUtilsDFGenerator project)

The relevant code files are copied into this folder. However, please be aware that the code will not build without the engine. Please let me know if you would like to compile the code on your (Windows) machine. I will then guide through the whole process OR I can provide binary files that do not require the engine.

(1) (this could not be done in time) cross-platform game engine with myself as the author - the aim is to port the necessary portions of the engine to Ubuntu for the final submission
