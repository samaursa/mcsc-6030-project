# README #

Real time, resolution independent text rendering is a non trivial problem. Current work uses high resolution bitmaps or vector curves to generate signed distance field (SDF) texture maps containing the requested glyphs of a particular font. Using SDF instead of a plain bitmap allows the text to be rendered independent of the resolution and can be scaled without causing aliasing or blurring. The proposed project will focus on parallelizing the generation and rendering of SDF font using `OpenMP` in `C`. Real time deformation of the SDF fonts will also be explored which can support effects such as text that is melting or on fire.

### Current State ###

* Project proposal
