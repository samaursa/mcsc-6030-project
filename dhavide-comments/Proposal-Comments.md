#MCSC6030G Project Proposal
##Saad Khattak

** **

## Recommendations for next checkpoint

+ Construct an appropriate title for the project
+ Some of the sentences read awkwardly. Be careful to use English idioms precisely in writing. This can be difficult to figure out in technical writing but please try.
+ + I'd prefer a more detailed description of the term *quad primitive* as well as *sprite sheet*
+ The nested captions within Figure 1 are confusing (particularly the sentence fragment in part (b)).
+ Below Figure 1(b): "...size of $50$ pixels". Is that supposed to mean $50\times50$ pixels or $50$ pixels in total?
+ I need some clarification of what you mean by "texture" and "rendered to a texture".
+ "...four nested loops and a complexity of $O(n^4)$..."
    + This needs a lot more explanation to follow why this is the case. Provide a pseudocode in the next write-up
    + What exactly does the variable $n$ refer to in this complexity bound? Be explicit.
    + $O(n^4)$ is not that punitive if $n$ is not large. How large does $n$ get?
    + This $n^4$ presumably refers to floating-point (or similar) operations. What about memory access patterns? Do those affect this computation? Is it possible to set it up in a cache-friendly way?
    + It is necessary to introduce variable names before using them in prose. In this instance, you don't really need to use $i$ and $j$ at all.
+ "Applying level set methods to deform the text..." Wait, where did level-set methods come from? You haven't mentioned them yet; how do they relate to this problem?
+ The comment about the non-thread-safe nature of OpenGL is important. Please explain how this limitation affects your proposed parallel code.
+ "These buffers can then be manipulated and updated in parallel and finally sent
to the GPU sequentially." I'm confused here; OpenMP doesn't work on GPUs. Are you planning on doing the generation of the glyphs on the host using threaded computation? Or is it something else? Explain carefully.
+ One thing I'd like to find out is what specifically you intend to do. I imagine you want to:
   + Construct an OpenMP implementation of the loops that generate the glyphs
   + Generate and run a set of tests/experiments to benchmark the OpenMP glyph generation against a conventional serial implementation. This requires some explanation about the sizes chosen.