\documentclass[10pt,a4paper]{article}
\usepackage{setspace}
\usepackage[latin1]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{subfigure}
\usepackage{listings}
\usepackage{xcolor}
\usepackage{multirow}

\definecolor{listinggray}{gray}{0.9}
\definecolor{lbcolor}{rgb}{0.9,0.9,0.9}
\lstset{
	backgroundcolor=\color{lbcolor},
	tabsize=2,
	captionpos=b,
	tabsize=3,
	frame=lines,
	numbers=left,
	numberstyle=\tiny,
	numbersep=5pt,
	breaklines=true,
	showstringspaces=false,
	basicstyle=\footnotesize,
    identifierstyle=\color{magenta},
	keywordstyle=\color[rgb]{0,0,1},
	commentstyle=\color{Darkgreen},
	stringstyle=\color{red}
}

\title{Real-time Parallax Deformable Text Using Distance Fields}
\author{Saad R. Khattak}

\begin{document}
	
	\maketitle
	
	\section{Abstract}
	\doublespacing
	
	Real-time resolution independent text rendering is a non trivial problem. Current work uses high resolution bitmaps or vector curves to generate signed distance field (SDF) texture maps containing the requested glyphs of a particular font. Using a SDF instead of a plain bitmap allows the text to be rendered independent of the resolution and can be scaled without causing aliasing or blurring. The proposed project will focus on parallelizing the generation and rendering of SDF fonts using $OpenMP$ in $C$. Real-time deformation of the SDF fonts is also explored which can support effects such as text that is melting or on fire (although the current implementation is not yet able to deform the text). Finally, to render the text on a 2D primitive while appearing 3D is done using steep parallax mapping where is a simple ray-tracer is built in the fragment shader.
	
	\section{Motivation}
	\doublespacing
	
	\begin{figure}[t]
		\centering
		\subfigure[]{
			\includegraphics[width=0.3\textwidth]{img/font_sprite_sheet}
		}
		~~
		\subfigure[]{
			\includegraphics[width=0.3\textwidth]{img/bitmap_font}
			\label{fig:bitmap_font_a}
		}		
		\caption{The font sprite sheet (a) is used to render each character using a textured quad (b). Since the glyph is rendered as a bitmap  Note that the green border is for illustration purposes only.}
		\label{fig:bitmap_font_rendering}
	\end{figure}
	
	Real-time text rendering is an important part of user interfaces often taken for granted. The most common technique to render text in real-time is to use quad primitives and a sprite sheet\footnote{a sprite sheet is a texture atlas where several textures are packed into one} (Figure~\ref{fig:bitmap_font_rendering}). The cost of rendering such fonts is very low as no extra work must be done on the GPU to render the text. In the absence of a GPU, a software renderer emulates a GPU using a CPU.
    
    Each character (glyph) of a particular font is initially rasterized with the help of libraries such as FreeType. The rasterization process produces clean, high fidelity glyphs of a particular size for a particular screen resolution (more specifically, the screen DPI). A change in resolution or font size requires the font to be rasterized again. Each size of the same font must be rasterized and placed into a sprite sheet.
	
	An obvious disadvantage to using bitmap textures for rendering fonts is aliasing issues. For example, in Figure~\ref{fig:bitmap_font_a}, the letter $a$ is rendered using a FreeType bitmap of width 50 pixels. The letter is then scaled and rasterized using the default linear interpolation provided by fragment shaders. The result is aliased and blurry. Thus, a font sprite sheet for each size must be prepared. Furthermore, bold fonts and other simple effects on the font such as outlining and rotations require additional rasterized glyphs to be stored in the font sprite sheet.
	
	Another technique that is becoming more popular for rendering text real-time is using signed distance field (SDF) instead of plain bitmaps. Figure~\ref{fig:sdf_font} shows the letter $a$ rendered using a $32\times32$ SDF. The quad on which the glyph is rendered is scaled to be $7$ times larger than the generated SDF grid. Despite the scale, the final rendering of the glyph is smooth and crisp compared to that in Figure~\ref{fig:bitmap_font_a}. Effects such as bold fonts, outline, glow, shadows etc. can be calculated and rendered using the original SDF of a particular glyph. More interesting effects can be generated using the same SDF texture such as text that is melting or on fire. Such effects can be very difficult to reproduce with bitmap fonts.
	
	SDF text rendering does suffer from some drawbacks. The initial generation of SDF glyphs (rendered to a texture) requires very high resolution bitmap text for calculating the signed distance. In the worst case, we have four nested loops and a complexity of $O(n^4)$ where $n$ is image width and/or height. 
    
    2 nested loops to get the current pixel at row $i$ and column $j$ of the source image; 2 nested loops to find the pixel with the shortest distance to the current pixel at $i,j$). Furthermore, rendering the font requires a specialized shader and special effects that require modification of the SDF will increase the rendering cost.
	
	Most of the disadvantages of SDF text rendering can be mitigated by \textbf{parallelizing} the generation, manipulation and rendering of the SDF glyphs.
	
	\begin{figure}[t]
		\centering
		
		\subfigure[]{
			\includegraphics[width=0.25\textwidth]{img/df_text_as_df}
		}
		~~
		\subfigure[]{
			\includegraphics[width=0.3\textwidth]{img/df_text}
		}
		~~
		\subfigure[]{
			\includegraphics[width=0.3\textwidth]{img/df_text_angle}
		}
		
		\caption{(a) a $32\times32$ texture encoding signed distances with red being outside, green inside and black on the surface, (b) the letter a rendered using the $32\times32$ SDF grid. The red box indicates the SDF grid size as compared to the size of the glyph render showing that large magnifications do not degrade the quality of the text, (c) the same letter rendered at an angle (spatial transformations on bitmap fonts degrades the quality of the text significantly)}
		\label{fig:sdf_font}
	\end{figure}
	
	\section{Related Work}
	\doublespacing
	
	Using SDFs for rendering text is not new. Qin et al \cite{texture_mapped_glyphs} showed the generation of SDF from vector shapes of glyphs. The sharp edges of the font are preserved by calculating a \textit{pseudodistance} instead of the euclidean distance which produces rounded corners. Green \cite{valve_paper} showed improvements to alpha-testing of the SDF to remove \textit{waviness} at high magnifications and used the technique in Valve's Source game engine. 
	
	The above mentioned techniques use an existing high resolution bitmap to generate a low resolution continuous SDF texture.	Glyphy \cite{glyphy}, an SDK written by Behdad, goes one step further by calculating the SDF using the original curves provided by true type fonts (TTF). 
	
	So far, the focus has been on rendering high quality SDF text in real-time. Applying level set methods to deform the text in a way that is suitable for real-time applications has, to my knowledge, yet to be done. In addition, this project explores the idea of rendering high quality 3D text on 2D surfaces using parallax mapping. This technique can allow for high quality real-time effects previously not possible with 2D text.
	
	\section{Tools and SDKs}
	\doublespacing
    
	The $C++$ programming language is being used for this project. The 2LoC cross-platform game engine is used for creating an $OpenGL$ context (window) and I/O. Multi-threading is done with the help of $OpenMP$ and some algorithms are implemented in $OpenGL$ shaders which are executed in parallel but asynchronously.
    
    \section{Method}
    \doublespacing
    
    There are three main parts to successfully rendering parallax deformable SDF text. The first step requires generating signed distance field textures of each glyph that is required. For example, the sentence \textit{"the quick brown fox jumps over the lazy dog"} contains all letters of the English alphabet and therefore requires SDF textures to be generated for all lower case letters (assuming a font type has been preselected). These textures should ideally be converted to a sprite sheet (Section~\ref{sec:sprite_sheets}) to reduce the number of textures that the GPU must sample. 
    
    The second step involves rendering the surface defined by the SDF by either interpolating across the surface boundary or by discovering the intersecting pixels and coloring them. The former technique produces smooth results and thus is more suitable for rendering. The latter technique is more precise and but may show aliasing and is thus more suitable for the intermediate deformation stage. Steep parallax mapping (with some modifications) is used to give the effect of 3D text on a 2D plane, i.e. without using any extra geometry \cite{main_parallax_mapping_paper}.
    
    The final step involves deformation of the text using LSM methods. As for this writing, the prototype produces incorrect deformation when leveraging GPU shaders for surface advection.
    
    \subsection{Generating SDF Textures}
    
    \begin{figure}[h]
        \label{lst:sdf_tool_help}
        \begin{verbatim}
        --help         Print usage and exit.
        -i <filename>, --input=<filename>   Input file for DF generation.
        -n             Inverses the incoming image.
        -o <filename>, --output=<filename>  Output file with SDF (PNG).
        -s,            Disallows overwriting existing files.
        -w,            Width of the final SDF image. Height is calculated 
        from ratio of source image
        -k,            DF is calculated upto this many pixels (in radius) 
        from the current pixel.
        \end{verbatim}
        \caption{SDF tool command line options}
    \end{figure}    
    
    To facilitate generating and examining SDF images a specialized tool was created which can also be run from the command line. The tool is able to generate a SDF from any black and white image source. Listing~\ref{lst:sdf_tool_help} shows the valid options for the tool. The options of interest are $-w$ and $-k$.
    
    Calculating the SDF of even small images is quite costly as 4 nested loops are involved (Listing~\ref{lst:nested_loops}). This is a prime candidate for $OpenMP$'s $parallel for$ construct (discussed in more detail in the Section~\ref{sec:openmp}). It is worth mentioning that a progress bar is also included which poses interesting challenges when parallelizing the code.
    
    \begin{lstlisting}[caption={Code showing the 4 nested loops for calculating SDF},label={lst:nested_loops}]
    for (tl_int row = 0; row < sdfImgWidth; row++) {
      for (tl_int col = 0; col < sdfImgHeight; col++) {
        /* code to get the color at [row, col] */
        for (tl_int kRow = -kernelSize; kRow <= kernelSize; ++kRow) {
          for (tl_int kCol = -kernelSize; kCol <= kernelSize; ++kCol) {
            /* code to calculate shortest distance from current pixel to pixel that intersects the surface */
          }
        }
      }
    }        
    \end{lstlisting}
    
    \subsection{Rendering SDF}
    
    There are two main parts to successfully rendering SDF text. The first step requires generating signed distance field textures of each glyph that is required. For example, the sentence \textit{"the quick brown fox jumps over the lazy dog"} contains all letters of the English alphabet and therefore requires SDF textures to be generated for all lower case letters (assuming a font type has been preselected). These textures should ideally be converted to a sprite sheet (Section~\ref{sec:sprite_sheets}) to reduce the number of textures that the GPU must sample. 
    
    The second step requires rendering each letter on a quad primitive using the SDF textures with the help of the fragment shader. The simplest implementation for rendering an SDF texture (as seen in Figure~\ref{fig:sdf_font}) is shown in Listing~\ref{lst:df_frag_shader}.
    
    \begin{lstlisting}[caption={Fragment shader for SDF rendering},label={lst:df_frag_shader}]
    in  vec2            v_texCoord;
    out vec4            o_color;   // final color of this fragment
    uniform sampler2D   s_texture; // texture sampler
    
    const float smoothing = 1.0/8.0;
    
    void main()
    {
        o_color = texture2D(s_texture, v_texCoord);
        
        float newDis = smoothstep(0.5 - smoothing, 0.5 + smoothing, o_color[0]);
        o_color = vec4(newDis, newDis, newDis, 1.0 - newDis);
    }
    \end{lstlisting}
    
    \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{img/parallax_mapping}
        
        \caption{Normal parallax mapping (left) where the incorrect intersection to the surface is calculated due to the crude approximation performed by the offset limiting method. The steep parallax method (right) segments the height map into equal steps. The ray from the camera is then traced and sampled at these steps. This ray-tracer is implemented in the fragment shader.}
        \label{fig:parallax_map}
    \end{figure}
    
    Once the surface contour has been established, information from the \textbf{signed} part of the distance field is used to define the height of the extrusion of the text and used to render the text in 3D on a 2D plane using steep parallax mapping \cite{main_parallax_mapping_paper}.
    
    To give the illusion of depth, the parallax effect algorithm calculates the approximate intersection point of the surface with an offset applied to the texture coordinates. The actual intersection will require a ray trace which is too costly. Instead, a single step approach is taken often known as parallax mapping with offset limiting \cite{main_parallax_mapping_paper}. The vector responsible for the offset is calculated using the height of the pixel at the current location (stored in a height map which can be a separate texture or stored in one of the channels of the diffuse texture) as the magnitude of the vector in the direction of the viewer (Figure~\ref{fig:parallax_map} Parallax Mapping). 
    
    \begin{figure}
        \centering
        
        \subfigure[]{
            \includegraphics[width=0.45\textwidth]{img/parallax_example_1}
        }
        \subfigure[]{
            \includegraphics[width=0.45\textwidth]{img/parallax_example_2}
        }
        \caption{(a) the results of the steep parallax rendering. Note that the depth of the cloth like texture does not use any additional geometry, as shown in (b) where the red lines show the two triangles that make up this particular quad primitive}
        \label{fig:parralax_example_1}
    \end{figure}
    
    At steep angles the parallax effect is lost as the approximation gets worse with higher viewing angles. To solve this issue, a simple ray-tracer is implemented in the fragment shader which calculates a more accurate intersection point by segmenting the height of the extrusion (Figure~\ref{fig:parallax_map} Steep Parallax Mapping). This allows calculating an offset that lies in between two heights. To avoid using too many segments, the algorithm presented in \cite{morganmcguiremaxmcguire2005} was modified to limit the total number of steps depending on the viewing angle. In this version, the number of steps is a function of viewing direction and the angle it forms with the surface (Equation~\ref{eq:steep_parallax}). The result is shown in Figure~\ref{fig:parralax_example_1}.  
    
    \begin{equation}
    \label{eq:steep_parallax}
    S(\vec{V}) = \frac{S_{max}}{\vec{V} \cdot \vec{N}}
    \end{equation}
    
    where $\vec{V}$ is the view direction vector, $\vec{N}$ the surface normal and $S_{max}$ the maximum number of steps allowed.
    
    \subsection{Deformation of SDF surfaces}
    
    \begin{figure}
        \centering
        
        \subfigure[]{
            \includegraphics[width=0.25\textwidth]{img/vector_field_1}
        }
        ~~
        \subfigure[]{
            \includegraphics[width=0.3\textwidth]{img/vector_field_2}
        }
        \caption{(a) a uniform velocity field that would push an implicit surface to the right (b) a velocity field that pushes an implicit surface towards the right but then converges to a particular area since the velocity changes direction}
        \label{fig:vec_field}
    \end{figure}
    
    The deformation of a SDF can be done by advection using velocity fields (Figure~\ref{fig:vec_field}). The advection process requires the gradient to be calculated at the point of advection. This point is a pixel with SDF textures where the color of the texture encodes the distance to the nearest point on the surface. The evolution of the implicit surface defined by the SDF can be done by the following integration 
    
    \begin{equation*}
    \label{eq:level_set_euler_equation}
    sdf_{new}(x,y) \approx sdf_{old}(x,y) - L(x,y) \cdot \Delta t 
    \end{equation*}
    
    \begin{equation*}
    \label{eq:level_set_equation}
    L = \vec{V} \cdot \nabla (x,y)
    \end{equation*}
    
    where $sdf_{new}$ and $sdf_{old}$ are the new and old SDFs, $\Delta t$ is the time between two successive frames, $\vec{V}$ is a vector from a known velocity field and $\nabla (x,y)$ the gradient. Solving for $L$ is called \textit{solving the level set equation}.
    
    To deform the SDF text, the velocity field can also be stored in a texture and passed to the fragment shader. The output of the fragment shader will be used as the SDF for the next iteration. Thus in addition to rendering to the screen, we will render the evolution of the SDF to another texture which will in turn be used for the next iteration. This process can also be done on the CPU although it is expected that the GPU accelerated version will be much faster.
    
   	\section{Parallelizing With OpenMP and GPU Shaders}
    \label{sec:openmp}
    \doublespacing
    
    \begin{figure}
        \centering
         \includegraphics[width=\textwidth]{img/deformation_pipeline}
        \caption{A high level view of the shader chain used for surface deformation where the fragment shaders are shown as boxes and textures as rounded rectangles. The \textit{front buffer} can be another texture or the screen.}
        \label{fig:deformation_pipeline}
    \end{figure}
    
    Parallelizing an algorithm with shaders is an exercise in frustration. One of the factors that makes the parallelism harder is the lack of debugging tools available. It is not possible to $printf()$ values or put breakpoints. The algorithm must be carefully thought out from start to finish since it will be executed in parallel asynchronously by thousands of fragment processors, each processing a unique fragment using the pixel shader. Nevertheless, steep parallax occlusion mapping was implemented successfully, allowing for any image with the appropriate normal and height maps to be rendered in 3D without using any additional geometry. Note that the height map and normal maps encode data (such as a surface normal vector) in color format. The encoding and decoding is usually at the shader level.
    
    Unlike steep parallax, the shader chain for surface deformation is more complex. At the start of the term, the plan was to perform all the calculations on the CPU. Slow results (even on smaller grids) in similar projects in the previous years led to the current usage of GPU shaders to deform the distance field. 
    
    Although the deformation itself does not work as expected yet, the formulation used (LSM methods) will remain the same and similar performance is expected once the bugs in the formulation are removed. To allow $OpenGL$ shaders to \textit{store} their results, the result must be rendered to another texture. The old SDF texture must now be updated with the new SDF and the new SDF is sent off to the next stage, which is using it as a texture for rendering a quad. Figure~\ref{fig:deformation_pipeline} shows an abstracted view of the shader chain.
    
    Parallelizing the code with $OpenMP$ was done relatively quickly as compared to GPU shaders. Listing~\ref{lst:nested_loops_omp} shows the parallelized version of the loops. The progress bar posed a challenge. The region responsible for printing the \textit{dashes} showing the progress was put under \lstinline{#pragma omp critical}. The $dashCount$ variable was incremented atomically using the \lstinline{#pragma omp atomic} directive.
    
    \begin{lstlisting}[caption={Code showing the 4 nested loops for calculating SDF},label={lst:nested_loops_omp}]
    #pragma omp parallel for shared(dashCount) num_threads(g_numOpenMPThreads)
    for (tl_int row = 0; row < sdfImgWidth; row++) {
      for (tl_int col = 0; col < sdfImgHeight; col++) {
        #pragma omp atomic
        dashCount++
        /* code to get the color at [row, col] */
        for (tl_int kRow = -kernelSize; kRow <= kernelSize; ++kRow) {
          for (tl_int kCol = -kernelSize; kCol <= kernelSize; ++kCol) {
            /* code to calculate shortest distance from current pixel to pixel that intersects the surface */
          }
        }
      }
    }        
    \end{lstlisting}
    
    A few different variations of the placement of the \lstinline{#pragma omp parallel for} were tried including parallelizing a small loop in the $GetAverageColorFromImg()$ function (responsible for averaging the colors in a given stencil). After profiling, the placement shown in in Listing~\ref{lst:nested_loops_omp} was the determined to be the most optimal.
    
    \section{Results}

    \begin{table}[h]
        \centering
        \begin{tabular}{|l|l|l|}
        	\hline
        	\textbf{\# of Threads} & \textbf{Texture Size} & \textbf{Time (sec)} \\ \hline
        	\multirow{5}{*}{1}     & $64\times64$          & $10.306876$           \\ \cline{2-3}
        	                       & $128\times128$        & $~8.611823$            \\ \cline{2-3}
        	                       & $256\times256$        & $~8.007860$             \\ \cline{2-3}
        	                       & $512\times512$        & $13.214408$           \\ \cline{2-3}
        	                       & $1024\times1024$      & $24.880820$           \\ \hline
        	\multirow{5}{*}{4}     & 64x64                 & $~3.675763$            \\ \cline{2-3}
        	                       & $128\times128$        & $~3.196193$            \\ \cline{2-3}
        	                       & $256\times256$        & $~3.460267$            \\ \cline{2-3}
        	                       & $512\times512$        & $~3.949625$            \\ \cline{2-3}
        	                       & $1024\times1024$      & $10.744831$           \\ \hline
        	\multirow{5}{*}{8}     & 64x64                 & $~2.617141$            \\ \cline{2-3}
        	                       & $128\times128$        & $~2.301692$            \\ \cline{2-3}
        	                       & $256\times256$        & $~1.976326$            \\ \cline{2-3}
        	                       & $512\times512$        & $~2.876040$            \\ \cline{2-3}
        	                       & $1024\times1024$      & $~7.840045$            \\ \hline
        \end{tabular}
        \caption{Results of SDF texture generation with different texture size outputs and number of threads used. The incoming texture has a size of $1024\times1024$ and the kernel (stencil) radius for all tests was chosen to be $16$.}
        \label{fig:sdf_generation_results}
    \end{table}
    
   	\begin{figure}
        \centering
        \subfigure[]{
            \includegraphics[width=0.3\textwidth]{img/b_sdf_64}
        }
        \subfigure[]{
            \includegraphics[width=0.3\textwidth]{img/b_sdf_128}
       }
       \subfigure[]{
           \includegraphics[width=0.3\textwidth]{img/b_sdf_256}
       }
       \subfigure[]{
           \includegraphics[width=0.3\textwidth]{img/b_sdf_512}
       }
       \subfigure[]{
           \includegraphics[width=0.3\textwidth]{img/b_sdf_1024}
       }		
       \caption{The output from the SDF generator with texture sizes of (a) $64\times64$ (b) $128\times128$ (c) $256\times256$ (d) $512\times512$ and (e) $1024\times1024$.  }
       \label{tab:sdf_generation_results}
    \end{figure}

    Parallelizing the generation of the SDF using $OpenMP$ yielded excellent results as shown in Table~\ref{tab:sdf_generation_results}. The tests were run on a \textit{Intel Core i7-2670QM CPU} running at $2.2GHz$ with $12GB$ of system memory with all compiler (VC++ 2013 Update 3) optimizations turned on. The resulting images, shown in Figure~\ref{fig:sdf_generation_results}, were free of any artifacts that may be the result of data races. One thing to note is that the output size of $256\times256$ seems to be faster on average than both $64\times64$ and $128\times128$. Intuitively this should not be the case. One possible explanation is that $OpenMP$ is able to make better use of the processing power available with a larger (but not too large) image size. The other reason may be a bug in the code (although the resulting images suggest that the code is correct).
    
    Deformation times for various grid sizes (determined by the texture image size) of the SDF are very encouraging. A $1024\times1024$ SDF grid takes on average $0.197ms$ to complete. Considering a total frame time allowance of $1.0/60.0sec$, the deformation consumes only $1.2 \%$ of the frame time. In fact the majority of the time is taken up copying the new SDF image data into the old SDF image data. Since this copying is done using $OpenGL$ buffers, it is not possible to parallelize using $OpenMP$. The deformation was performed on various grid (texture) sizes as shown in Table~\ref{tab:sdf_deformation_results}.
    
    \begin{table}[h]
        \begin{tabular}{|l|l|l|}
            \hline
            \textbf{Texture Size} & \textbf{Time to Deform (ms)} & \textbf{Total Time (ms)} \\ \hline
            $64\times64$                     & 0.198                        & 0.832                    \\ \hline
            $128\times128$                   & 0.195                        & 1.275                    \\ \hline
            $256\times256$                   & 0.199                        & 3.319                    \\ \hline
            $512\times512$                   & 0.187                        & 10.863                   \\ \hline
            $1024\times1024$                 & 0.182                        & 41.932                   \\ \hline
        \end{tabular}
        \caption{This table shows the time taken to deform the grid using a velocity field of the same size as well as the total time for the complete operation which includes deformation and copying of the new SDF data (texture) into the old SDF buffer (texture).}
        \label{tab:sdf_deformation_results}
    \end{table}
    
    \begin{figure}
        \centering
        
        \subfigure[]{
            \includegraphics[width=0.45\textwidth]{img/parallax_example_3}
        }
        \subfigure[]{
            \includegraphics[width=0.45\textwidth]{img/parallax_example_4}
        }
        \caption{Similar to Figure~\ref{fig:parralax_example_1} but this time with SDF text.}
        \label{fig:parralax_example_2}
    \end{figure}
    
    The result for parallax rendering of a SDF is shown in Figure~\ref{fig:parralax_example_2}. Some artifacts can be seen due to sharp angles. Steep parallax mapping does not take advantage of SDF. Better results with fewer artifacts can be produced by a method proposed in \cite{gpu_gems}. 
    
    \section{Discussion and Future Work}

    The results of parallelizing computationally intensive code using $OpenMP$ and $GPU$ shaders are quite encouraging. Barring the bug in the deformation shader, it is clear that the complete process of generating the SDF of bitmap text, deforming the SDF and rendering it in parallax 3D can be performed online\footnote{online is defined as a process that can be performed in a reasonable amount of time without waiting too long for the result (which is then classified as offline)} (as opposed to offline) and the latter two stages performed in real-time.
    
    The first task to be completed in the near future is correct deformation of the distance field. Currently, all the stages exist as separate projects which should be converted into a single library that generates a glyph cache of all the required letters, packs them into a single texture using a modern bin packing algorithm and provides shaders for deforming and rendering the SDF text.
    
    \section{Appendix}
    
    \subsection{Primitive}
    
    The simplest geometric object that a graphics system can render. GPUs can only handle triangle primitives although modern APIs such as DirectX and OpenGL allow rendering other primitive types such as points and lines (see Figure~\ref{fig:primitives}). Note that the points and lines are also made up of triangles.
    
    \begin{figure}[h]
    	\centering
   		\includegraphics[width=\textwidth]{img/geometric_primitives}
  	
    	\caption{Various primitives that can be submitted to the OpenGL graphics API}
    	\label{fig:primitives}
    \end{figure}
    
    \subsection{Texture}
    \label{sec:texture}
        
    An image applied to a 3D primitive. The standard way to apply a texture to a 3D primitive is by using 2D texture coordinates which are the coordinates of an unwrapped 3D surface onto a 2D plane. The 2D coordinates are usually represented with a 2D vector $[u, v]$ where $0<=u<=1$ and $0<=v<=1$.
    
    \subsection{Quad}
    \label{sec:quad}
    
    Graphics engines can render different kinds of primitives to make up geometric shapes. One such primitive is a Quad and is made up of two triangles (Figure~\ref{fig:quad}). Although there is no restriction on the placement of the 4 vertices of a quad primitive, the quad is usually shaped as a rectangle. Quads are commonly used for rendering transparent textures such as font glyphs.
    
    \begin{figure}[h]
    	\centering
    	\includegraphics[width=0.5\textwidth]{img/quad_primitive}
    	
    	\caption{A texture applied on a quad primitive with V0, V1, V2 and V3 as the vertices of the quad.}
    	\label{fig:quad}
    \end{figure}
    
    \subsection{Sprite Sheet}
    \label{sec:sprite_sheets}
    
    A sprite sheet (Figure~\ref{fig:bitmap_font_rendering}(a)) is a single image that contains multiple textures and is always accompanied by a descriptor (usually a text file) specifying the starting coordinate, width and height of a particular texture. These coordinates can then be used to generate texture coordinates for a triangle and passed to the fragment shader for surface rendering.
    
    \subsection{Shader Program}
    \label{sec:shader_program}
    
    A shader program compiles and links different shaders for use in rendering OpenGL primitives. At a minimum a vertex and fragment shader are required.
    
    \subsection{Vertex Shader}
    \label{sec:vertex_shader}
    
    Part of the programmable OpenGL pipeline which is responsible for processing each vertex of a primitive. Outputs from a vertex shader are interpolated across the surface and the interpolated value is passed to the fragment shader.
    
    \subsection{Fragment Shader}
    \label{sec:fragment_shader}
    
    Part of the programmable OpenGL pipeline responsible for processing a fragment (pixel) from the rasterization process with color as output. The color is usually in the RGBA (red, green, blue, alpha) format.
    
    \subsection{Uniform Variable}
    \label{sec:uniform_variable}
    
    Uniform variables are shader variables that remain constant throughout the rendering of a single primitive. More specifically, a uniform variable remains constant for the duration of a draw call.
    
    \subsection{Attribute Variable}
    \label{sec:attribute_variable}
    
    Attribute variables or vertex attributes are shader variables that change per vertex; i.e. each iteration of the vertex shader may (or may not) receive a different value depending on the nature of the draw call. In C, the vertex attributes are arrays where each element (usually) corresponds to one vertex and thus one pass of the vertex shader
    
	\bibliographystyle{elsarticle-num}
	\bibliography{bib/references}
	
\end{document}

\iffalse

- Real-time text rendering limitations
- SDF text rendering benefits and limitations

2 problems
- Fast generation of the SDF font sprite sheet
- Fast rendering of SDF font

\fi